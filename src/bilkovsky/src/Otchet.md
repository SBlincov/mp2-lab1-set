"Множество на основе битовых полей"
#1.Описание работы

#1.1  Цель работы:разработка структуры данных для хранения множеств с
использованием битовых полей, а также освоение таких инструментов разработки
программного обеспечения, как система контроля версий git и фрэймворк для
разработки автоматических тестов Google Test.

#1.2  Выполнение работы предполагает:

1. Реализация класса битового поля __`TBitField`__ согласно заданному интерфейсу.
2. Реализация класса множества __`TSet`__ согласно заданному интерфейсу.
3. Обеспечение работоспособности тестов и примера использования.
4. Реализация нескольких простых тестов на базе Google Test.
5. Публикация исходных кодов в личном репозитории на BitBucket

#2 Реализация битового поля

#2.1 Описание класса TBitField :
Битовое поле представленно в виде массива pMem с типом TELEM (unsigned int)
BitLen - количество  битов в нашем поле,на основе  BitLen вычисляется MemLen - количество элементов pMem.
В классе реализованны методы доступа к отдельным битам, операции над битовыми полями и операторы ввода/выводы.
Нумерация битов идет справа налево,первый бит имеет индекс (0),а последний (Bitlen-1).

Работа программы предполагает обработку исключений:
1 - неккорктный ввод данных для создания объекта
2 - не выделилась память для объекта
3 - Неккоректный индекс,выход за границы поля

#2.2 Реализация класса TBitField

!Заголовочный файл "tbitfield.h" был получене перед выполнением работы

#include "tbitfield.h"
inline int Counter()
{
	int count = 0, size = sizeof(TELEM) * 8;
	while (size >>= 1)
		count++;
	return count;
}

TBitField::TBitField(int len)
{
	if (len < 0)
		throw 1;
	BitLen = len;
	MemLen = (BitLen + sizeof(TELEM) * 8 - 1) >> Counter();
	pMem = new TELEM[MemLen];
	if (pMem == nullptr)
		throw 2;
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (bf.pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n >= BitLen || n < 0)
		throw 3;
	return n >> Counter();
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n >= BitLen || n < 0)
		throw 3;
	return 1 << (n % (sizeof(TELEM) * 8));
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n >= BitLen || n < 0)
		throw 3;
	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n >= BitLen || n < 0)
		throw 3;
	pMem[GetMemIndex(n)] &= (~GetMemMask(n));
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n >= BitLen || n < 0)
		throw 3;
	return (bool)(pMem[GetMemIndex(n)] & GetMemMask(n));
}
// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	delete[] pMem;
	pMem = new TELEM[MemLen];
	if (bf.pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if(BitLen==bf.BitLen)
	{
		for (int i = 0; i < BitLen; i++)
			if (GetBit(i) != bf.GetBit(i))
				return 0;
			return 1;
	}
	return 0;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if (BitLen == bf.BitLen)
	{
		for (int i = 0; i < BitLen; i++)
			if (GetBit(i) != bf.GetBit(i))
				return 1;
		return 0;
	}
	return 1;
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	int len = BitLen;
	if (bf.BitLen > BitLen)
		len = bf.BitLen;
	TBitField temp(len);
	for (int i = 0; i < MemLen; i++)
		temp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		temp.pMem[i] |= bf.pMem[i];
	return temp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	int len = BitLen;
	if (bf.BitLen > BitLen)
		len = bf.BitLen;
	TBitField temp(len);
	for (int i = 0; i < MemLen; i++)
		temp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		temp.pMem[i] &= bf.pMem[i];
	return temp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField temp(BitLen);
	for (int i = 0; i < MemLen; i++)
		temp.pMem[i] = ~pMem[i];
	return temp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	int i = 0,l = 1; char bit;
	while (l & (i < bf.BitLen))
	{
		istr >> bit;
		switch (bit)
		{
			case'0':
				bf.ClrBit(i++);
				break;
			case'1':
				bf.SetBit(i++);
				break;
			case' ':
				break;
			default:
				l = 0;
				break;
		}
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << '1';
		else
			ostr << '0';
	return ostr;
}

#2.3 Опиание класса Tset

Tset - множества на основе битовых полей. MaxPowr - максимальная мощность множества, BitField - экземпляр класса TBitField. В классе реализованы методы работы с элементами множества,оператор преабразования типа, операторы ввода/вывода.
Исключения : 
4 - обращение к несуществющему элементу множества

#2.4 Реализация класса Tset:

заголовочный файл "tset.h" был получен перед выполнением работы

#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return BitField;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	if(Elem >= MaxPower || Elem < 0)
		throw 4;
    return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	if(Elem >= MaxPower || Elem < 0)
		throw 4;
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	if(Elem >= MaxPower || Elem < 0)
		throw 4;
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if(BitField == s.BitField)
		return 1;
    return 0;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	if(BitField == s.BitField)
		return 0;
	return 1;
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet temp(BitField | s.BitField);
	return temp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet temp = *this;
	temp.InsElem(Elem);
	return temp;
}
TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet temp = *this;
	temp.DelElem(Elem);
	return temp;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet temp(BitField & s.BitField);
	return temp;
}

TSet TSet::operator~(void) // дополнение
{
	TSet temp = ~BitField;
	return temp;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	int el;
	char br;
	while(true)
	{
		istr >> el;
		s.InsElem(el);
		istr >> br;
		if(br != ',')
			break;
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	bool first = true;
	ostr <<"{";	
	for(int i = 0; i < s.MaxPower; i++)
	{
		if(s.IsMember(i))
		{
			if(!first)
				ostr <<"; ";
			else
				first = false;
			ostr << i;
		}
	}
	ostr <<"}";
	return ostr;
}

#3. Тестирование

#3.1 Написание тестов:

К уже имеющимся тестам добавим 3 своих :
1)Проврка корректности битовой операции в одну строку с несколькими полями
2)Проверка двойной очистки бита
3)Проверка корректности сложения в одну строку нескольких множеств


TEST(TBitField, or_bitfield) 
{
	int size = 5;
	TBitField bf1(size),bf2(size),bf3(size),res1(size),res2(size);
	bf1.SetBit(0);
	bf2.SetBit(0);
	bf3.SetBit(3);
	bf3.SetBit(4);
	res1 = bf1 | bf2 | bf3;
	res2.SetBit(0);
	res2.SetBit(3);
	res2.SetBit(4);
	EXPECT_EQ(res1, res2);
}
TEST(TBitField, 2nd_time_cleanig)
{
	TBitField bf(5);
	bf.SetBit(3);
	bf.ClrBit(3);
	EXPECT_EQ(0, bf.GetBit(3));
	bf.ClrBit(3);
	EXPECT_EQ(0, bf.GetBit(3));	
}
TEST(TSet, plus_tset) 
{
	int size = 5;
	TSet set1(size),set2(size),set3(size),res1(size),res2(size);
	set1.InsElem(0);
	set2.InsElem(0);
	set3.InsElem(3);
	set3.InsElem(4);
	res1 = set1 + set2 + set3;
	res2.InsElem(0);
	res2.InsElem(3);
	res2.InsElem(4);
	EXPECT_EQ(res1, res2);
}

#3.2 Результаты тестирования

Все тесты, включая три написанных самостоятльно пройдены.
Показательная программа "Решето Эратосфена" основанная на битовом поле работает.

#4 Вывод:

В ходе выполнения работы были реализованы два класса TBitField  и TSet, для этого были исользованы некоторые возможности ООП (перегрузка операций,дружественные функции, наследование). Так же были изучены основы тестирования на базе Google Test. Из последнего можем сделать вывод,что тестирование является полезным инструментом,но гарантий правильности программы не дает.