// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"
#include <cmath> // Подключаем для возможности использовать log();

const unsigned short BDigit = sizeof(TELEM)*8;	 
const unsigned short Shift = ceil(log((double)BDigit)/log(2.0));


TBitField::TBitField(int len)
{
	if (len < 1)
	{
		throw "\n\nError 1! Inaccessible length of the bitfield!\n\n";
	}

	BitLen = len;
	MemLen = (BitLen + (BDigit - 1)) >> Shift;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
	pMem[i] = 0;

}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
	pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	return n >> Shift;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	return (1 << (n % (sizeof(TELEM) * 8)));
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	pMem[GetMemIndex(n)] &= (~GetMemMask(n));
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n >= BitLen || n < 0)
	{
		throw "\n\nError 2! This bit does not exist!\n\n";
	}

	return (bool)(pMem[GetMemIndex(n)] & GetMemMask(n));

}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	delete[] pMem;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];

	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (BitLen == bf.BitLen)
	{
		for (int i = 0; i < BitLen; i++)
		if (GetBit(i) != bf.GetBit(i))
			return 0;
		return 1;
	}
	else
		return 0;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if (BitLen == bf.BitLen)
	{
		for (int i = 0; i < BitLen; i++)
		if (GetBit(i) != bf.GetBit(i))
			return 1;
		return 0;
	}
	else
		return 1;
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	int len = BitLen;

	if (bf.BitLen > BitLen)
		len = bf.BitLen;

	TBitField data(len);

	for (int i = 0; i < MemLen; i++)
		data.pMem[i] = pMem[i];

	for (int i = 0; i < bf.MemLen; i++)
		data.pMem[i] |= bf.pMem[i];

	return data;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	{
	int len = BitLen;

	if (bf.BitLen > BitLen)
		len = bf.BitLen;

	TBitField data(len);

	for (int i = 0; i < MemLen; i++)
		data.pMem[i] = pMem[i];

	for (int i = 0; i < bf.MemLen; i++)
		data.pMem[i] &= bf.pMem[i];

	return data;
}
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField temp(BitLen);
	for (int i = 0; i < MemLen; i++)
	temp.pMem[i] = ~pMem[i];
	return temp;	

}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char data;
	
	for (int i = 0; i<bf.BitLen; i++)
	{
		tryagain:
		istr >> data;
		if ( (data == '1') || (data = '0') )

			if (data == '1')
				bf.SetBit(i);
			else
				bf.ClrBit(i);

		else
			cout<<"\b";
			goto tryagain;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
for (int i = 0; i < bf.BitLen; i++)
if (bf.GetBit(i))
ostr << '1';
else
ostr << '0';
return ostr;
}